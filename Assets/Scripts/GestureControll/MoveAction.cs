﻿using Academy.HoloToolkit.Unity;
using UnityEngine;

public class MoveAction : MonoBehaviour {

    public float MoveSpeed = 0.1f;
    private Vector3 manipulationPreviousPosition;

    //private void PerformMoving()
    //{
    //    if (GestureManager.Instance.IsNavigating && HandsManager.Instance.FocusedGameObject == gameObject)
    //    {
    //        Debug.Log("You wanna move the box?");
    //    }
    //}

    void PerformManipulationStart(Vector3 position)
    {
        manipulationPreviousPosition = position;
    }

    void PerformManipulationUpdate(Vector3 position)
    {
        if (GestureManager.Instance.IsManipulating)
        {
            Debug.Log("You are moving the cube.");
            Vector3 moveVector = Vector3.zero;

            // 4.a: Calculate the moveVector as position - manipulationPreviousPosition.
            moveVector = position - manipulationPreviousPosition;

            // 4.a: Update the manipulationPreviousPosition with the current position.
            manipulationPreviousPosition = position;

            // 4.a: Increment this transform's position by the moveVector.
            transform.position += moveVector;
        }
    }

    public enum ManipulationType { Position = 0, Scale = 1 };
    private bool _manipulating;
    private Vector3 _lastManipulationPosition;
    private ManipulationType _manipulationType;

    public void StartLocationManipulation(Vector3 pos)
    {
        _manipulationType = ManipulationType.Position;
        StartManipulation(pos);
    }

    void Update()
    {
        if (_manipulating)
        {
            UpdateManipulation();
            _manipulating = GestureManager.Instance.IsManipulating;
        }
    }

    //public void StartScaleManipulation(Vector3 pos)
    //{
    //    _manipulationType = ManipulationType.Scale;
    //    StartManipulation(pos);
    //}

    private void StartManipulation(Vector3 pos)
    {
        _lastManipulationPosition = pos;
        _manipulating = true;
    }

    public void UpdateManipulation()
    {
        Vector3 pos = GestureManager.Instance.ManipulationPosition;
        Vector3 diff = _lastManipulationPosition - pos;

        switch (_manipulationType)
        {
            //case (ManipulationType.Scale):
            //    Vector3 currentScale = transform.localScale;
            //    currentScale += diff;
            //    transform.localScale = currentScale;
            //    break;
            case (ManipulationType.Position):
                Vector3 currentPos = transform.position;
                currentPos -= diff;
                transform.position = currentPos;
                break;
        }

        _lastManipulationPosition = pos;

        _manipulating = GestureManager.Instance.IsManipulating;
    }
}
